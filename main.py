import requests
from pydantic import BaseModel, ValidationError
from typing import Optional

API_KEY = "cb0904c92f40771e5cd7ed7a45bb84b8"
REQUEST_TEMPLATE = "http://api.openweathermap.org/data/2.5/weather?q={}&appid={}"
CURRENT_WEATHER = """
    current weather in {}
    {}
    temperature {} F 
    feels like {} F 
    wind sped {} mps 
"""


class Clouds(BaseModel):
    all: int


class Coord(BaseModel):
    lat: float
    lon: float


class Main(BaseModel):
    feels_like: float
    humidity: int
    pressure: int
    temp: float
    temp_max: float
    temp_min: float


class Rain(BaseModel):
    lh: Optional[float] = None


class Sys(BaseModel):
    country: str
    id: int
    sunrise: int
    sunset: int
    type: int


class Weather(BaseModel):
    description: str
    icon: str
    id: int
    main: str


class Wind(BaseModel):
    deg: int
    speed: int


class RecWeather(BaseModel):
    base: str
    clouds: Clouds
    cod: int
    coord: Coord
    dt: int
    id: int
    main: Main
    name: str
    rain: Rain
    sys: Sys
    timezone: int
    visibility: int
    weather: list[Weather]
    wind: Wind


curr_weather = requests.get(REQUEST_TEMPLATE.format('Moscow', API_KEY))

try:
    rec_weather = RecWeather.parse_raw(curr_weather.content)
except ValidationError as err:
    print(err.json())
else:
    result = CURRENT_WEATHER.format(rec_weather.name, rec_weather.weather[0].description,
                                    rec_weather.main.temp, rec_weather.main.feels_like,
                                    rec_weather.wind.speed)
    print(result)
